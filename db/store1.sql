-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 26, 2018 at 10:23 AM
-- Server version: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store1`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_pesan`
--

CREATE TABLE IF NOT EXISTS `tbl_detail_pesan` (
  `id_detail_pesan` int(11) NOT NULL,
  `id_pesanan` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `catatan_produk` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_konsumen`
--

CREATE TABLE IF NOT EXISTS `tbl_konsumen` (
  `id_konsumen` int(11) NOT NULL,
  `nama_konsumen` varchar(200) NOT NULL,
  `email_konsumen` varchar(200) NOT NULL,
  `password_konsumen` varchar(200) NOT NULL,
  `no_hp_konsumen` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_konsumen`
--

INSERT INTO `tbl_konsumen` (`id_konsumen`, `nama_konsumen`, `email_konsumen`, `password_konsumen`, `no_hp_konsumen`) VALUES
(1, 'Eko Rismaryanto', 'eko.rme@gmail.com', '3fb9e98095c6c9e9c0f86df8c37fc526', '082377673248');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesanan`
--

CREATE TABLE IF NOT EXISTS `tbl_pesanan` (
  `id_pesanan` int(11) NOT NULL,
  `id_konsumen` int(11) NOT NULL,
  `nama_penerima` varchar(200) NOT NULL,
  `no_hp_penerima` varchar(200) NOT NULL,
  `provinsi_penerima` varchar(200) NOT NULL,
  `kota_penerima` varchar(200) NOT NULL,
  `kode_pos` char(200) NOT NULL,
  `alamat_penerima` text NOT NULL,
  `jumlah_ongkir` int(11) NOT NULL,
  `jumlah_pesan` int(11) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `kode_unix` char(200) NOT NULL,
  `tanggal_pesan` date NOT NULL,
  `status_pesanan` enum('Menunggu','Konfirmasi','Dikonfirmasi') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk`
--

CREATE TABLE IF NOT EXISTS `tbl_produk` (
  `id_produk` int(11) NOT NULL,
  `nama_produk` varchar(200) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `stok_produk` enum('Tersedia','Kosong') NOT NULL,
  `deskripsi_produk` text NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `gambar_1` varchar(200) NOT NULL,
  `gambar_2` varchar(200) NOT NULL,
  `gambar_3` varchar(200) NOT NULL,
  `gambar_4` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_produk`
--

INSERT INTO `tbl_produk` (`id_produk`, `nama_produk`, `id_kategori`, `stok_produk`, `deskripsi_produk`, `harga_produk`, `gambar_1`, `gambar_2`, `gambar_3`, `gambar_4`) VALUES
(1, 'Produk 1', 1, 'Tersedia', '23123', 10000, 'uploads/file_1535033731.jpg', '', '', ''),
(3, 'ankmdalksd akjsd klasjdkl', 1, 'Tersedia', '123123123', 20000, 'uploads/file_1535085503.jpg', '', '', ''),
(4, 'ankmdalksd akjsd klasjdkl', 1, 'Tersedia', '123123123', 20000, 'uploads/file_1535085503.jpg', '', '', ''),
(5, 'ankmdalksd akjsd klasjdkl', 1, 'Tersedia', '123123123', 20000, 'uploads/file_1535085503.jpg', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk_kategori`
--

CREATE TABLE IF NOT EXISTS `tbl_produk_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_produk_kategori`
--

INSERT INTO `tbl_produk_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Mobil');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_detail_pesan`
--
ALTER TABLE `tbl_detail_pesan`
  ADD PRIMARY KEY (`id_detail_pesan`);

--
-- Indexes for table `tbl_konsumen`
--
ALTER TABLE `tbl_konsumen`
  ADD PRIMARY KEY (`id_konsumen`);

--
-- Indexes for table `tbl_pesanan`
--
ALTER TABLE `tbl_pesanan`
  ADD PRIMARY KEY (`id_pesanan`);

--
-- Indexes for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `tbl_produk_kategori`
--
ALTER TABLE `tbl_produk_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_detail_pesan`
--
ALTER TABLE `tbl_detail_pesan`
  MODIFY `id_detail_pesan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_konsumen`
--
ALTER TABLE `tbl_konsumen`
  MODIFY `id_konsumen` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_pesanan`
--
ALTER TABLE `tbl_pesanan`
  MODIFY `id_pesanan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_produk_kategori`
--
ALTER TABLE `tbl_produk_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
