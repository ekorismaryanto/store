<style type="text/css">
.tabs-left, .tabs-right {
	border-bottom: none;
	padding-top: 2px;
}
.tabs-left {
	border-right: 1px solid #ddd;
}
.tabs-right {
	border-left: 1px solid #ddd;
}
.tabs-left>li, .tabs-right>li {
	float: none;
	margin-bottom: 2px;
}
.tabs-left>li {
	margin-right: -1px;
}
.tabs-right>li {
	margin-left: -1px;
}
.tabs-left>li.active>a,
.tabs-left>li.active>a:hover,
.tabs-left>li.active>a:focus {
	border-bottom-color: #ddd;
	border-right-color: transparent;
}

.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
	border-bottom: 1px solid #ddd;
	border-left-color: transparent;
}
.tabs-left>li>a {
	border-radius: 4px 0 0 4px;
	margin-right: 0;
	display:block;
}
.tabs-right>li>a {
	border-radius: 0 4px 4px 0;
	margin-right: 0;
}
.vertical-text {
	margin-top:50px;
	border: none;
	position: relative;
}
.vertical-text>li {
	height: 20px;
	width: 120px;
	margin-bottom: 100px;
}
.vertical-text>li>a {
	border-bottom: 1px solid #ddd;
	border-right-color: transparent;
	text-align: center;
	border-radius: 4px 4px 0px 0px;
}
.vertical-text>li.active>a,
.vertical-text>li.active>a:hover,
.vertical-text>li.active>a:focus {
	border-bottom-color: transparent;
	border-right-color: #ddd;
	border-left-color: #ddd;
}
.vertical-text.tabs-left {
	left: -50px;
}
.vertical-text.tabs-right {
	right: -50px;
}
.vertical-text.tabs-right>li {
	-webkit-transform: rotate(90deg);
	-moz-transform: rotate(90deg);
	-ms-transform: rotate(90deg);
	-o-transform: rotate(90deg);
	transform: rotate(90deg);
}
.vertical-text.tabs-left>li {
	-webkit-transform: rotate(-90deg);
	-moz-transform: rotate(-90deg);
	-ms-transform: rotate(-90deg);
	-o-transform: rotate(-90deg);
	transform: rotate(-90deg);
}
</style>


<section id="shopgrid" class="shop shop-grid">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
				
				<!-- <table class="table table-condensed" style="color: black">
					<thead>
						<tr>
							<th>Tanggal Pesan</th>
							<th>Nama Penerima</th>
							<th>alamat Penerima</th>
							<th>Total Bayar</th>
							<th>Kurir Pengiriman</th>
							<th>Total Ongkir</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($pesanan as $r): ?>
							
							<tr>
								<td><?php echo $r->tanggal_pesan ?></td>
								<td><?php echo $r->nama_penerima ?></td>
								<td><?php echo $r->alamat.', '.$r->provinsi_penerima.', '.$r->kota_penerima.'<br>'. $r->kode_pos ?></td>
								<td>Rp. <?php echo number_format($r->total_bayar) ?></td>
								<td><?php echo $r->kurir ?></td>
								<td>Rp. <?php echo number_format($r->jumlah_ongkir) ?></td>
							</tr>

						<?php endforeach ?>

					</tbody>
				</table> -->


				
				<h3>Pesanan</h3>
				<hr/>
				<?php foreach ($pesanan as $r): ?>

					<div class="col-md-12">
						<div class="col-xs-3">
							<ul class="nav nav-tabs tabs-left">
								<li class="active"><a href="#home<?php echo $r->id_pesanan ?>" data-toggle="tab">Status Pesanan</a></li>
								<li><a href="#profile<?php echo $r->id_pesanan ?>" data-toggle="tab">Detail Pesanan</a></li>
								<li><a href="#messages<?php echo $r->id_pesanan ?>" data-toggle="tab">Cara Pembayaran</a></li>
							</ul>
						</div>
						<div class="col-xs-9">
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active" id="home<?php echo $r->id_pesanan ?>"></div>
								<div class="tab-pane" id="profile<?php echo $r->id_pesanan ?>">
									
									<table cellpadding="6" cellspacing="1" style="width:100%" border="0">

										<tr>
											<th>QTY</th>
											<th>Produk</th>
											<th style="text-align:right">Harga</th>
											<th style="text-align:right">Sub-Total</th>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td colspan="3" class="right"><strong>Total</strong></td>
											<td  class="right" align="right">$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
										</tr>

									</table>
									
									<p><?php echo form_submit('', 'Update your Cart'); ?></p>
									

									<?php echo $r->alamat.', '.$r->provinsi_penerima.', '.$r->kota_penerima.'<br>'. $r->kode_pos ?>	
								</div>
								<div class="tab-pane" id="messages<?php echo $r->id_pesanan ?>">Messages Tab.</div>
							</div>
						</div>

						<hr>
					</div>
				</table>
				<hr/>

			<?php endforeach ?>

			<div class="clearfix"></div>



		</div>
	</div>
</div>
</section>