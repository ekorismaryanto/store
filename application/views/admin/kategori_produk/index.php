
    <div class="au-card-inner">
    	<h3 class="title-5 m-b-35">Data Kategori Produk</h3>
    	<div class="table-data__tool">
    		<div class="table-data__tool-left"></div>
    		<div class="table-data__tool-right">

    			<a href="<?php echo site_url('admin/kategori_produk/tambah_produk') ?>">
    				
    				<button class="au-btn au-btn-icon au-btn--green au-btn--small">
    					<i class="zmdi zmdi-plus"></i>Tambah Produk</button></a>

    					<div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
    						<select class="js-select2" name="type">
    							<option selected="selected">Export</option>
    							<option value="">Option 1</option>
    							<option value="">Option 2</option>
    						</select>
    						<div class="dropDownSelect2"></div>
    					</div>


    				</div>
    			</div>
    			<div class="table-responsive table-responsive-data2">
    				<table id="example4" class="display" style="width:100%">
    					<thead>
    						<tr>
    							<th>Name</th>
    							<th>Position</th>
    							<th>Office</th>
    							<th>Age</th>
    							<th>Start date</th>
    							<th>Salary</th>
    						</tr>
    					</thead>
    					<tbody>
    						<tr>
    							<td>Tiger Nixon</td>
    							<td>System Architect</td>
    							<td>Edinburgh</td>
    							<td>61</td>
    							<td>2011/04/25</td>
    							<td>$320,800</td>
    						</tr>
    					</tbody>
    				</table>
    			</div>
    			<!-- END DATA TABLE -->
    		</div>


    		<script type="text/javascript">
    			$(document).ready(function() {
    				$('#example4').DataTable();
    			} );
    		</script>