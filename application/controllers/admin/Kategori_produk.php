<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_produk extends CI_Controller {

	public function index()
	{
		$data['content'] = $this->load->view('admin/kategori_produk/index','',true);
		$this->load->view('admin/template',$data);
	}

	public function tambah_produk()
	{
		$data['content'] = $this->load->view('admin/kategori_produk/tambah','',true);
		$this->load->view('admin/template',$data);
	}

	public function simpan()
	{
		$this->db->insert('tbl_produk_kategori', array('nama_kategori' => $this->input->post('nama_kategori')));
		redirect('admin/kategori_produk','refresh');
	}

}

/* End of file Kategori_produk.php */
/* Location: ./application/controllers/admin/Kategori_produk.php */