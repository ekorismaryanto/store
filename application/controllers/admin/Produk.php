<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url','download'));                          
	}

	public function index()
	{
        $data['produk'] = $this->db
        ->select('*,tbl_produk_kategori.nama_kategori')
        ->join('tbl_produk_kategori', 'tbl_produk.id_kategori=tbl_produk_kategori.id_kategori')
        ->get('tbl_produk')->result();
        $tmp['content'] = $this->load->view('admin/produk/index',$data,true);
        $this->load->view('admin/template',$tmp);
    }

    public function tambah_produk()
    {
      $data['kategori'] = $this->db->get('tbl_produk_kategori')->result();
      $tmp['content'] = $this->load->view('admin/produk/tambah_produk', $data ,true);
      $this->load->view('admin/template',$tmp);
  }

  public function edit($id_produk)
  {
    $data['produk'] = $this->db->select('*,tbl_produk_kategori.nama_kategori')
    ->join('tbl_produk_kategori', 'tbl_produk.id_kategori=tbl_produk_kategori.id_kategori')
    ->where('id_produk',$id_produk)
    ->get('tbl_produk')->result();
    $data['kategori'] = $this->db->get('tbl_produk_kategori')->result();
    $tmp['content'] = $this->load->view('admin/produk/edit', $data ,true);
    $this->load->view('admin/template',$tmp);
}

public function simpan()
{
    $this->load->library('upload');
    $nmfile = "file_".time();
        $config['upload_path'] = './uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '3000'; //lebar maksimum 1288 px
        $config['max_height']  = '3000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if($_FILES['pic']['name'])
        {
            if ($this->upload->do_upload('pic'))
            {
                $url = $this->upload->data();
                $data_produk = array(
                    'nama_produk' => $this->input->post('nama'),
                    'harga_produk' => $this->input->post('harga'), 
                    'stok_produk' => $this->input->post('stok'),
                    'deskripsi_produk' => $this->input->post('deskripsi'),                                  
                    'id_kategori' => $this->input->post('kategori'),    
                    'gambar_1' => 'uploads/'.$url['file_name']                                  
                );
                $this->db->insert('tbl_produk', $data_produk);
                redirect('admin/produk/tambah_produk');
            }
        }
    }

    public function ubah_simpan($id_produk)
    {
        $this->load->library('upload');
        $nmfile = "file_".time();
        $config['upload_path'] = './uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '3000'; //lebar maksimum 1288 px
        $config['max_height']  = '3000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
        $this->upload->initialize($config);

        if ($this->upload->do_upload('pic'))
        {
            $url = $this->upload->data();
            $data_produk = array(
                'nama_produk' => $this->input->post('nama'),
                'harga_produk' => $this->input->post('harga'), 
                'stok_produk' => $this->input->post('stok'),
                'deskripsi_produk' => $this->input->post('deskripsi'),                                  
                'id_kategori' => $this->input->post('kategori'),    
                'gambar_1' => 'uploads/'.$url['file_name']                                  
            );
            $this->db->where('id_produk', $id_produk)->update('tbl_produk', $data_produk);
        }else{
            $data_produk = array(
                'nama_produk' => $this->input->post('nama'),
                'harga_produk' => $this->input->post('harga'), 
                'stok_produk' => $this->input->post('stok'),
                'deskripsi_produk' => $this->input->post('deskripsi'),                                  
                'id_kategori' => $this->input->post('kategori'),    
            );
            $this->db->where('id_produk', $id_produk)->update('tbl_produk', $data_produk);
        }
        redirect('admin/produk');
    }

    public function hapus($id_produk)
    {
        $this->db->where('id_produk', $id_produk);
        $this->db->delete('tbl_produk');
        redirect('admin/produk','refresh');
    }




}

/* End of file Produk.php */
/* Location: ./application/controllers/admin/Produk.php */